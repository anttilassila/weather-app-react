# Weather App

- https://anttilassila.bitbucket.io/
- https://youtu.be/Y65188vNKM4


**App feature summary**

- App lists finnish golf courses (static list, since no public api available, and creating a backend for that isn't the main thing in thjis assignment)
- You can search golf courses (with search engine that has autocomplete and fuzzy. Autocomplete is "hand made")
- At golf course page you can tag course as favorite, check the current temperature and forecast
- Favorites are stored in browser's localStorage

**Technical summary**

- App uses Redux-minimal React-boilerplate
- App Uses Redux-Saga state management
- App uses React-router
- App uses browser's localStorage as storage
- App uses Immutable for data collections
- App uses Bootstrap
- App uses Sass for css preprocessing
- Boilerplate uses webpack and babel
- App is not deployed to gitlab pages, since I use Bitbucket version control
- App has responsive design

**Personal summary**

- This was my first React app
- Since I have used Vue and I'm familiar with that, I felt that it was relatively easy to start using React
- Redux-Saga state management was by far the hardest part, since it wasn't that "intuitive" after been normally using Vue. But with trial and error method / google I managed to get on top of it
- After this I feel quite confident to start using React with real life projects

**Time tracking**

- 10.11. - Planning and wireframing - 3h
- 09.12. - Selecting and testing boilerplates - 2h
- 10.12. - Setting up version control and app base 3h
- 11.12. - API stuff and search component 4 h
- 12.12. - Implementing the original Golf Weather App idea, Search autocomplete 4 h
- 13.12. - Redux stuff, course page and weather data 4 h
- 14.12. - Weather data and data visualization, current weather 3 h
- 15.12. - Current weather, minor refactoring, favorite functionality 2 h
- 16.12. - Favorite functionality, responsive design, finalizing, publishing, documenting 3 h

Total time spent: 28 h