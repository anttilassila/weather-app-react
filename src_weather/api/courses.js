import Immutable from 'immutable';

const STORAGE = window.localStorage;

// API Courses static class
export default class CoursesApi {

  // get a list of courses
  static getCourses() {

      //this is static list, since there is no public API for this, and backend stuff isn't the main thing in this assignment 
      let coursedata = [
        {
          name: 'Jyväs-Golf ',
          location: 'Jyväskylä',
          fmisid: '101339',
          courseid: 'jg',
          favorite: 0
        },
        {
          name: 'Muurame Golf ',
          location: 'Muurame',
          fmisid: '101339',
          courseid: 'mg',
          favorite: 0
        },
        {
          name: 'Revontuli Golf ',
          location: 'Hankasalmi',
          fmisid: '101339',
          courseid: 'reg',
          favorite: 0
        },
        {
          name: 'Peurunkagolf ',
          location: 'Laukaa',
          fmisid: '101339',
          courseid: 'lpg',
          favorite: 0
        },
        {
          name: 'Himos Golf ',
          location: 'Jämsä',
          fmisid: '101362',
          courseid: 'hg',
          favorite: 0
        },
        {
          name: 'PuulaGolf ',
          location: 'Kangasniemi',
          fmisid: '101367',
          courseid: 'pg',
          favorite: 0
        }
      ];

      //get user favorites from device's localstrage
      const favs = CoursesApi.getFavorites();

      if(favs) {
        //loop through course data and update user's favorites
        coursedata.map((course, index) => {
          if(favs.get(course.courseid)) {
            coursedata[index].favorite = 1;
          }
        });
      }

      //return course data as Immutable List
      return Immutable.fromJS(coursedata);
  }

  //get favorites from device's localStorage
  static getFavorites () {
    const favs = JSON.parse(STORAGE.getItem('favorites'));
    if(favs) {
      //Return data as Immutable Map
      return Immutable.fromJS(favs);
    } else {
      return null;
    }
  }

  //save favorites to device's localStorage
  static setFavorites (favs) {
    STORAGE.setItem('favorites', JSON.stringify(favs));
  }

  //toggle course's favorite status
  static toggleFavorite(courseid, favstatus) {

    //get all favorites as Immutable Map
    let favs = CoursesApi.getFavorites();

    //return false if for some reason trying to remove course from favorites and there are no favorites
    if(!favs && !favstatus) {
      return false;
    }

    let newFavs;

    if(favstatus) {
      if(!favs) {
        //create new object if this is first favorite
        newFavs = {[courseid]: 1};
      } else {
        //add this course favs Map
        newFavs = favs.set(courseid, 1);
      }
    } else {
      //delete this course from favorites Map
      newFavs = favs.delete(courseid);
    } 

    //save updated favorites to localStorage
    CoursesApi.setFavorites(newFavs);

    //return true when favorites are updated
    return true;
  }
  
}
