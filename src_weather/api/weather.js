import * as fmi from "./fmi";
import Immutable from 'immutable';

// API Users static class
export default class WeatherApi {
  
  static getCourseWeather(fmisid) {
    return fmi.getCurrentWeather(fmisid);
  }

  static getCourseForecast(location) {
    return fmi.getForecast(location);
  }

}

