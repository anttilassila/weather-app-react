import React from "react";
import { connect } from "react-redux";
import { ProgressBar } from "react-bootstrap";
import Menu from "./common/Menu";
import Search from "./common/Search";
import "../stylesheets/main.scss";

// App component
export class App extends React.Component {

  // pre-render logic
  componentWillMount() {
    // the first time we load the app, we need that courses list
    this.props.dispatch({type: 'FETCH_COURSES'});
  }

  // render
  render() {
    
    const {courses, children} = this.props;

    // show the loading state while we wait for the app to load
    if (!courses) {
      return (
        <ProgressBar active now={100}/>
      );
    }
    
    // render
    return (
      <div className="page">
        <div className="header">
          <div className="container">
            <div className="header-wrapper">
              <Menu/>
              <Search/>
            </div>
          </div>
        </div>
        <div className="container">
          {children}
        </div>
      </div>
    );
  }
}

// export the connected class
function mapStateToProps(state) {
  return {
    courses: state.courses || []
  };
}
export default connect(mapStateToProps)(App);
