import React from "react";
import { connect } from "react-redux";
import CourseHeaderElement from "./common/CourseHeaderElement";
import CourseWeather from "./common/CourseWeather";
import CourseForecast from "./common/CourseForecast";

// Course page component
export class Course extends React.Component {

  // render
  render() {
    const courseid = this.props.params.id;
    const {courses} = this.props;
    //get course data from courselist stored in state
    const course = courses.find((course) => {
      return course.get('courseid') === courseid;
    });

    if(!courses || !course.size) {
      return null;
    }

    const location = course.get('location');
    const fmisid = course.get('fmisid');
    return (
      <div className="page-course">
        <CourseHeaderElement course={course} type="page" />
        <CourseWeather fmisid={fmisid} />
        <CourseForecast location={location} />
      </div>
    );
  }
}

// export the connected class
function mapStateToProps(state) {
  return {
    courses: state.courses || []
  };
}
export default connect(mapStateToProps)(Course);