import React from "react";
import { Jumbotron } from "react-bootstrap";
import Favorites from "./common/Favorites";

// Home page component
export default class Home extends React.Component {
  // render
  render() {
    return (
      <div className="page-home">
        <Jumbotron>
          <h2>Keskisuomen Golfsää</h2>
          <p>Palvelulla voit hakea keskisuomalaisten* golfkenttien säätietoja.</p>
          <div>* Palvelu on rajattu keskisuomalaisiin golfkenttiin, koska aika ei riittänyt lisäämään rajapintaan kaikkia Suomen golfkenttiä.</div>
        </Jumbotron>
        <Favorites/>
      </div>
    );
  }
}