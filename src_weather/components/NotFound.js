import React from "react";
import { Jumbotron } from "react-bootstrap";

// Not found page component
export default class NotFound extends React.Component {
  // render
  render() {
    return (
      <div className="page-not-found">
        <Jumbotron>
          <h2>Sivua ei löytynyt.</h2>
          <p>Hups. Etsimääsi sisältöä ei löytynyt. Käytä hakua tai palaa etusivulle.</p>
        </Jumbotron>
      </div>
    );
  }
}