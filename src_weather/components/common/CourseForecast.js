import React from "react";
import { connect } from "react-redux";
import { ProgressBar, Jumbotron } from "react-bootstrap";
import ForecastItem from "./ForecastItem";

//Course weather forecast component
export class CourseForecast extends React.Component {

  componentDidMount() {
    //fetch forecast data
    this.fetchWeatherData();
  }

  componentDidUpdate(prevProps) {
    //fetch new forecast data if course page has changed
    if(prevProps.location != this.props.location) {
      this.fetchWeatherData();
    }
  }

  fetchWeatherData () {
    //fetc forecast data
    this.props.dispatch({type: 'FETCH_COURSE_FORECAST', location: this.props.location});
  }

  // render
  render() {

    const {forecast} = this.props;

    let count = 0;
    
    //set weekdays
    const weekdays = [
      'Sunnuntai',
      'Maanantai',
      'Tiistai',
      'Keskiviikko',
      'Torstai',
      'Perjantai',
      'Lauantai'
    ];

    //handle loading status
    let loading = false;
    if(!forecast || !forecast.size) {
      loading = true;
    }

    let day = '';
    let dateHtml = '';
    // show the forecast
    return (
      <Jumbotron className="course-weather">
        <h2>Sääennuste</h2>
        {loading && 
          <ProgressBar className="progress" active now={100}/>
        }
        {!loading && forecast.map((weatheritem, index) => {
            count++;
            let d = new Date(weatheritem.get('time'));
            //group forecast items by date
            if(day != d.toLocaleDateString()) {
              //set the date header element
              day = d.toLocaleDateString();
              let wday = d.getDay();
              dateHtml = <div className="weather-date"><h3>{weekdays[wday]} {day}</h3></div>;
            } else {
              dateHtml = '';
            }
            return (
              <div className="weather-item-container">
              {dateHtml}
              <ForecastItem key={index} weather={weatheritem} />
              </div>
            );
          })
        }
        {!loading && !count &&
          <div className="notification">Hups. Säätila ei ole tällä hetkellä saatavilla.</div>
        }        
      </Jumbotron>
    );
  }

}

// export the connected class
function mapStateToProps(state) { 
  return {
    forecast: state.forecast || null
  };
}
export default connect(mapStateToProps)(CourseForecast);