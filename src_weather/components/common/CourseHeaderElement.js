import React from "react";
import { connect } from "react-redux";
import { Glyphicon, Jumbotron } from "react-bootstrap";
import { Link } from "react-router";

//Course header element component
export class CourseHeaderElement extends React.Component {

  // constructor
  constructor(props) {
    super(props);
  }
  
  //handle favorite button clicks
  toggleFavorite (event) {
    const course = this.props.course;
    const courseid = course.get('courseid');
    const fav = course.get('favorite');
    //toggle favorite status
    this.props.dispatch({type: 'TOGGLE_COURSE_FAVORITE', courseid: courseid, favstatus: !fav});
  }

  // render
  render() {

    const type = this.props.type;
    const course = this.props.course;
    
    if(!course) {
      return null;
    }

    const courseid = course.get('courseid');

    //handle favorite status
    const fav = course.get('favorite');
    let favicon;
    if(fav) {
      favicon = <Glyphicon glyph="star" />;
    } else {
      favicon = <Glyphicon glyph="star-empty" />;
    }

    //render different html structure and data, whether element is in list or in course page
    if(type == 'page') {
      return (
        <Jumbotron className="course-page-header">
          <h1 className="course-name">
            <span>{course.get('name')}</span>
            <div className="course-favorite" onClick={this.toggleFavorite.bind(this)}>{favicon}</div>  
          </h1>
          <div className="course-location">{course.get('location')}</div>
        </Jumbotron>
      );
    } else {
      return (
        <Link to={'/kentta/' + courseid} className="course-header">
          <div className="course-favorite">{favicon}</div>
          <div className="course-name">{course.get('name')}</div>
          <div className="course-location">{course.get('location')}</div>
          <div className="course-link">
            <Glyphicon glyph="chevron-right" />
          </div>
       </Link>
      )
    }
  }
}

// export the connected class
export default connect()(CourseHeaderElement);
