import React from "react";
import { connect } from "react-redux";
import { ProgressBar } from "react-bootstrap";
//import { push } from "react-router-redux";
import CourseHeaderElement from "./CourseHeaderElement";
import Fuse from 'fuse-immutable';


export class CourseList extends React.Component {

  // render
  render() {
    const {courses} = this.props;
    let courselist;

    //set the course list type
    const type = this.props.type || 'search';
    const searchword = this.props.searchword || '';

    //show loading bar while course list is still loading
    if (!courses) {
      return (
        <ProgressBar active now={100}/>
      );
    }
    
    //do the search if listing courses in search results
    if(type == 'search' && searchword) {

      //set Fuse.js options
      const fuseoptions = {
        threshold: 0.3,
        location: 0,
        distance: 100,
        maxPatternLength: 32,
        minMatchCharLength: 3,
        keys: [
          "name",
          "location"
      ]
      }
       
      //do the search and set the courselist to be shown
      const fuse = new Fuse(courses, fuseoptions)
      courselist = fuse.search(searchword);
    } else {
      //use the courselist from state, if not search
      courselist = courses;
    }

    //show hint if no search results
    if (!courselist.size && type == 'search') {
      return (<div className="notification">Ei hakutuloksia. Miten ois vaikka Muurame tai Laukaa?</div>);
    }
    
    let count = 0;
    // show the list of courses
    return (
      <div className="course-list">
      {courselist.map((course, index) => {
        //create course items
        //show only favorites, if list type is favorites, otherwise show all
        if(type != 'favorites' || course.get('favorite') == 1) {
          count++;
          return (
            <CourseHeaderElement key={index} course={course} type={type} />
          );
        } else {
          return null;
        }
      })}
      {type == 'favorites' && !count &&
        <div className="notification">Et ole merkinnyt vielä yhtään kenttää suosikiksi.</div>
      }
      </div>
    );
  }

}

// export the connected class
function mapStateToProps(state) {
  return {
    courses: state.courses,
  };
}
export default connect(mapStateToProps)(CourseList);
