import React from "react";
import { connect } from "react-redux";
import { ProgressBar, Jumbotron } from "react-bootstrap";

export class CourseWeather extends React.Component {

  componentDidMount() {
    //fetch course weather
    this.fetchWeatherData();
  }

  componentDidUpdate(prevProps) {
    //Check if course page has changed
    if(prevProps.fmisid != this.props.fmisid) {
      //update weather data when course page has changed
      this.fetchWeatherData();
    }
  }

  fetchWeatherData () {
    //fetch course weather
    this.props.dispatch({type: 'FETCH_COURSE_WEATHER', fmisid: this.props.fmisid});
  }

  // render
  render() {

    const {weather} = this.props;
    let currentWeather = null;
    let temperature;
    let loading = false;
    //show loading bar while weather data is still loading
    if(!weather || !weather.size) {
      loading = true;
    } else {
      //use the latest weather observation
      currentWeather = weather.last();
      temperature = parseInt(currentWeather.get('t2m'), 10);
    }
    
    // show the current weather
    return (
      <Jumbotron className="course-weather">
        <h2 className="current-weather-title">Lämpötila tällä hetkellä
        {!loading && temperature &&
          <span className="current-temperature">{temperature}&deg;</span>
        }
        </h2>
        {loading && 
          <ProgressBar className="progress" active now={100}/>
        }
      </Jumbotron>
    );
  }

}

// export the connected class
function mapStateToProps(state) { 
  return {
    weather: state.weather || null
  };
}
export default connect(mapStateToProps)(CourseWeather);