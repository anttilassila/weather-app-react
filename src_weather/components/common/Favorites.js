import React from "react";
import { Jumbotron } from "react-bootstrap";
import CourseList from "./CourseList";

// Menu component
export default class Favorites extends React.Component {
  // render
  render() {
    return (
      <div className="favorites">
        <Jumbotron>
          <h2>Suosikit</h2>
          <CourseList type="favorites"/>       
        </Jumbotron>
      </div>
    );
  }
}
