import React from "react";
import { Glyphicon } from "react-bootstrap";

// Menu component
export default class ForecastItem extends React.Component {

  // render
  render() {

    const weather = this.props.weather;

    //format timestamp to HH:MM format
    const d = new Date(weather.get('time'));
    let hours = d.getHours();
    let minutes = d.getMinutes();

    if(hours < 10) {
      hours = '0' + hours;
    };

    if(minutes < 10) {
      minutes = '0' + minutes;
    };

    //format windspeed
    let wind = weather.get('WindSpeedMS');
    wind = parseFloat(wind).toFixed(1);

    //set css rotation to wind direction arrow
    const windDir = weather.get('WindDirection');
    const wstyle = {
      transform: 'rotate(-'+windDir+'deg)'
    };

    //format temperature
    const temperature = parseInt(weather.get('Temperature'), 10);

    //set weather icon
    const icon = '/weather_icons/'+parseInt(weather.get('WeatherSymbol3'), 10)+'.svg';

    return (
      <div className="weather-item">
        <div className="weather-stamp">{hours + ':' + minutes}</div>
        <div className="weather-icon"><img src={icon} alt="" /></div>
        <div className="weather-temperature">{temperature}&deg;</div>
        <div className="weather-wind">{wind} m/<sup>s</sup> <Glyphicon glyph="arrow-up" style={wstyle} /></div>
      </div>
    );
  }
}
