import React from "react";
import { Nav, NavItem, Glyphicon } from "react-bootstrap";
import { IndexLinkContainer } from "react-router-bootstrap";

// Menu component
export default class Menu extends React.Component {
  // render
  render() {
    return (
      <div className="menu">
        <Nav bsStyle="pills">
          <IndexLinkContainer to="/">
            <NavItem>
              <Glyphicon glyph="home" />
            </NavItem>
          </IndexLinkContainer>
        </Nav>
      </div>
    );
  }
}
