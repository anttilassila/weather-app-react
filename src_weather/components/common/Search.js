import React from "react";
import CourseList from "./CourseList";
import {browserHistory} from 'react-router';

// Search component
export default class Search extends React.Component {

  constructor(props) {
    super(props);

    // default ui local state
    this.state = {
      searchword: '',
      show_results: false
    };

    //bind this to click handler
    this.handleClickOutsideSearch = this.handleClickOutsideSearch.bind(this);
  }

  componentDidMount() {
    //listen if page changes and clear searchfield if it does
    this.unlisten = browserHistory.listen( location =>  {
      this.setState({searchword: ''});
    });

  }
  componentWillUnmount() {
    this.unlisten();
  }

  //Search input handler
  handleSearchField(event) {

    let val = event.target.value;
    //keep input calue in state
    this.setState({ searchword: val });

    //show search results if searchword is long enough
    if(val.length >= 3) {
      this.setState({show_results: true});
      //listen if user click's outside the search component
      document.addEventListener('mousedown', this.handleClickOutsideSearch);
    } else {
      this.setState({show_results: false});
      //remove listener since it's not needed at the time
      document.removeEventListener('mousedown', this.handleClickOutsideSearch);
    }
  }

  handleResultsClick(event) {
    //hide result list after user has clicked a result item/link
    this.setState({show_results: false});
  }


  handleClickOutsideSearch (event) {
    //Hide result list if user click'c outside the search component
    const mainsearch = document.getElementById('mainsearch');
    if(!mainsearch.contains(event.target)) {
      this.setState({show_results: false});
      document.removeEventListener('mousedown', this.handleClickOutsideSearch);
    }
  }
  

  // render
  render() {
    const placeholder = 'Hae keskisuomalaisen golfkentän nimellä tai paikkakunnalla';
    
    return (
      <div id="mainsearch" className="search">
        <div className="search-field">
          <input type="search" value={this.state.searchword} onChange={this.handleSearchField.bind(this)} onFocus={this.handleSearchField.bind(this)} placeholder={placeholder} />
        </div>
        {this.state.show_results &&
          <div className="search-results" onClick={this.handleResultsClick.bind(this)}><CourseList searchword={this.state.searchword} type="search"/></div>
        }
      </div>
    );
  }
}
