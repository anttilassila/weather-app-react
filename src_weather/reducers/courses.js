import CoursesApi from "../api/courses";

// course reducer
export default function courses(state = {}, action) {
  switch (action.type) {
    case 'COURSES_SAVE':
      //save courselist to state
      return action.data;
    case 'COURSE_FAVORITE_SAVE':
      if(!action) {
        //no updates to favorites so return old course list
        return state;
      } else {
        //update new favoritestatus to course list stored in state
        const newstate = state.update(
          state.findIndex(function(course) { 
            return course.get("courseid") === action.data.courseid; 
          }), function(course) {
            return course.set("favorite", action.data.favstatus);
          }
        ); 
        return newstate;
      } 
    // initial state
    default:
      return state;
  }
}