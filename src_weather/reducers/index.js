import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
//import { reducer } from "redux-form";
import courses from "./courses";
import {weather, forecast} from "./weather";

// main reducers
export const reducers = combineReducers({
  routing: routerReducer,
  courses,
  weather,
  forecast
});
