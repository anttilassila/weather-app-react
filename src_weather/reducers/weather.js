// weather reducer
function weather(state = {}, action) {
  switch (action.type) {
    case 'COURSE_WEATHER_SAVE':
      return action.data;
    // initial state
    default:
      return state;
  }
}

// forecast reducer
function forecast(state = {}, action) {
  switch (action.type) {
    case 'COURSE_FORECAST_SAVE':
      return action.data;
    // initial state
    default:
      return state;
  }
}



export { weather, forecast }