import { call, put } from "redux-saga/effects";
import CoursesApi from "../api/courses";

// fetch the course list
export function* fetchCourses(action) {
  // call the api to get the course list
  const courses = yield call(CoursesApi.getCourses);

  // save the courses in state
  yield put({
    type: 'COURSES_SAVE',
    data: courses,
  });

}

// toggle course favorite status
export function* toggleCourseFavorite(action) {
  
  // call the api to toggle course favorite status
  let response = yield call(CoursesApi.toggleFavorite, action.courseid, action.favstatus);

  ///if favorites were updated, use original action object as return data 
  if(response) {
    response = action;
  }

  // save the updated courses state in state
  yield put({
    type: 'COURSE_FAVORITE_SAVE',
    data: response,
  });

}