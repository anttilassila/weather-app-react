import { takeLatest } from "redux-saga";
import { fork } from "redux-saga/effects";
import { fetchCourses, toggleCourseFavorite } from "./courses";
import { fetchCourseWeather, fetchCourseForecast } from "./weather";

// main saga generators
export function* sagas() {
  yield [
    fork(takeLatest, 'FETCH_COURSES', fetchCourses),
    fork(takeLatest, 'TOGGLE_COURSE_FAVORITE', toggleCourseFavorite),
    fork(takeLatest, 'FETCH_COURSE_WEATHER', fetchCourseWeather),
    fork(takeLatest, 'FETCH_COURSE_FORECAST', fetchCourseForecast)
  ];
}
