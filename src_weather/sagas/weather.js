import { call, put } from "redux-saga/effects";
import WeatherApi from "../api/weather";

// fetch the course weather
export function* fetchCourseWeather(action) {

  //delete old state while fetching updates
  yield put({
    type: 'COURSE_WEATHER_SAVE',
    data: null,
  });

  // call the api to get the current weather list
  const weather = yield call(WeatherApi.getCourseWeather, action.fmisid);

  // save weather in state
  yield put({
    type: 'COURSE_WEATHER_SAVE',
    data: weather,
  });

}

// fetch the course weather forecast
export function* fetchCourseForecast(action) {

  //delete old state while fetching updates
  yield put({
    type: 'COURSE_FORECAST_SAVE',
    data: null,
  });

  // call the api to get the forecast list
  const forecast = yield call(WeatherApi.getCourseForecast, action.location);

  // save forecast in state
  yield put({
    type: 'COURSE_FORECAST_SAVE',
    data: forecast,
  });

}